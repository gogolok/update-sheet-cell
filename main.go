package main

import (
	"gitlab.com/gogolok/update-sheet-cell/cmd"
)

func main() {
	cmd.Execute()
}
