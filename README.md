# update-sheet-cell

Update a specific cell of a Google sheet with a value.

## Usage

```shell
export SERVICE_ACCOUNT="service@account.com"
SPREADSHEET_ID="1Wd6KR9Ex9wm-zb7eicEdx1TMlimJZg2nf41ygIRFSEA"
export PRIVATE_KEY="-----BEGIN PRIVATE KEY-----
MIIE3gIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC5PshmGpPaDo3r
...
-----END PRIVATE KEY-----"

$ go run main.go
Spreadsheet with ID 1Wd6KR9Ex9wm-zb7eicEdx1TMlimJZg2nf41ygIRFSEA Cell Portfolio!B49 has been updated with value 835.91.
```
