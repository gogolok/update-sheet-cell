package cmd

import (
	"strconv"

	"gitlab.com/gogolok/update-sheet-cell/pkg/updater"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(setCmd)
}

var setCmd = &cobra.Command{
	Use:  "set cell-location float-value",
	Args: cobra.ExactArgs(2),
	RunE: setCell,
}

func setCell(cmd *cobra.Command, args []string) error {
	cell := args[0]
	valueStr := args[1]

	value, err := strconv.ParseFloat(valueStr, 64)
	if err != nil {
		return err
	}

	return updater.SetCellValue(cell, value)
}
