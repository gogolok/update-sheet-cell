package updater

import (
	"context"
	"errors"
	"fmt"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2/jwt"
	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

func SetCellValue(writeRange string, value float64) error {
	serviceAccount := os.Getenv("SERVICE_ACCOUNT")
	if len(serviceAccount) < 1 {
		return errors.New("Environment variable SERVICE_ACCOUNT not set")
	}

	spreadsheetId := os.Getenv("SPREADSHEET_ID")
	if len(spreadsheetId) < 1 {
		return errors.New("Environment variable SPREADSHEET_ID not set")
	}

	privateKey := os.Getenv("PRIVATE_KEY")
	if len(privateKey) < 1 {
		return errors.New("Environment variable PRIVATE_KEY not set")
	}

	conf := &jwt.Config{
		Email:      serviceAccount,
		PrivateKey: []byte(privateKey),
		Scopes: []string{
			"https://www.googleapis.com/auth/spreadsheets",
		},
		TokenURL: google.JWTTokenURL,
	}

	client := conf.Client(oauth2.NoContext)

	srv, err := sheets.NewService(context.Background(), option.WithHTTPClient(client), option.WithScopes(sheets.SpreadsheetsScope))
	if err != nil {
		return fmt.Errorf("Unable to retrieve Google Sheets client: %v", err)
	}

	// Write a call
	var vr sheets.ValueRange
	values := []interface{}{value}
	vr.Values = append(vr.Values, values)

	_, err = srv.Spreadsheets.Values.Update(spreadsheetId, writeRange, &vr).ValueInputOption("RAW").Do()
	if err != nil {
		return fmt.Errorf("Unable to update values: %v", err)
	}
	fmt.Printf("Spreadsheet with ID %v Cell %v has been updated with value %v.\n", spreadsheetId, writeRange, value)

	return nil
}
